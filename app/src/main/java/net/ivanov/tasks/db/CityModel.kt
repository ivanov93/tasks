package net.ivanov.tasks.db

import com.vicpin.krealmextensions.AutoIncrementPK
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class CityModel() : RealmObject() {
    @PrimaryKey
    @AutoIncrementPK
    var id: Long = 0
    var city: String = ""
    var page: Int = 0
    var saved: Boolean = false

    constructor(id: Long, city: String, page: Int, saved: Boolean) : this() {
        this.city = city
        this.page = page
        this.saved = saved
        this.id = id
    }
}