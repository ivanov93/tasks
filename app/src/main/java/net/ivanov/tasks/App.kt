package net.ivanov.tasks

import android.app.Application
import android.os.Parcel
import android.os.Parcelable
import com.vicpin.krealmextensions.RealmConfigStore
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmConfiguration
import net.ivanov.tasks.db.CityModel
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

class App : Application() {
    lateinit var api: Api

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        val dbConf = RealmConfiguration.Builder().name("city-db")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build()
        RealmConfigStore.init(CityModel::class.java, dbConf)
        api = Retrofit.Builder()
                .baseUrl("http://murzinma.ru/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(OkHttpClient().newBuilder()
                        .readTimeout(15, TimeUnit.SECONDS)
                        .writeTimeout(15, TimeUnit.SECONDS)
                        .connectTimeout(15, TimeUnit.SECONDS)
                        .build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build().create(Api::class.java)
    }
}

interface Api {
    @GET("api/people.php")
    fun getUserData(): Observable<Response<UserData>>

    @GET("api/cities.php")
    fun getCities(@Query("page") page: Int): Observable<Response<Cities>>

    @GET("api/dates.php")
    fun getDates(): Observable<Response<Dates>>

    @GET("api/counter.php")
    fun counter(@Query("delta") delta: Int, @Query("action") action: String): Observable<Response<Counter>>
}

data class UserData(val name: String, val birthday: String, val description: String)
data class Cities(val error: Int, val pages: Int, val page: Int, val cities: List<String>)
data class Dates(val past: List<DateEvent>, val future: List<DateEvent>)
data class DateEvent(val time: Long, val description: String)
data class Counter(val value:Int, val error: Int)

class ParcDateEventList() : Parcelable {
    lateinit var list: List<DateEvent>

    constructor(parcel: Parcel) : this() {
        this.list = parcel.readValue(List::class.java.classLoader) as List<DateEvent>
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(list)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ParcDateEventList> {
        override fun createFromParcel(parcel: Parcel): ParcDateEventList {
            return ParcDateEventList(parcel)
        }

        override fun newArray(size: Int): Array<ParcDateEventList?> {
            return newArray(size)
        }
    }
}