package net.ivanov.tasks.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_user.*
import net.ivanov.tasks.App
import net.ivanov.tasks.R

class UserActivity : AppCompatActivity() {
    val TAG = "UserActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        title = "User"
        supportActionBar?.setDisplayShowHomeEnabled(true)
        getUserData()
        repeat.setOnClickListener {
            getUserData()
        }
        refresh.setOnClickListener {
            getUserData()
        }
    }

    private fun getUserData() {
        error.visibility = View.GONE
        user_data.visibility = View.GONE
        progress.visibility = View.VISIBLE
        (application as App).api.getUserData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d(TAG, "getUserData response code: ${it.code()}")
                    if (it.isSuccessful) {
                        val user = it.body()
                        Log.d(TAG, "getUserData: $user")
                        name.text = user.name
                        birthday.text = user.birthday
                        description.text = user.description
                        progress.visibility = View.GONE
                        user_data.visibility = View.VISIBLE
                    } else {
                        handleError()
                    }
                }, {
                    Log.e(TAG, "onCreate: ", it)
                    handleError()
                })

    }

    private fun handleError() {
        progress.visibility = View.GONE
        error.visibility = View.VISIBLE
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy: ")
    }
}
