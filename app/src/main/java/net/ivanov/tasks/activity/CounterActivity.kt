package net.ivanov.tasks.activity

import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_counter.*
import net.ivanov.tasks.App
import net.ivanov.tasks.R

class CounterActivity : AppCompatActivity() {
    val TAG = "CounterActivity"
    private var counterValue = 0
    private var lastAction = "increment"
    private lateinit var countdownTimer: CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_counter)
        title = "Counter"
        supportActionBar?.setDisplayShowHomeEnabled(true)
        countdownTimer = object : CountDownTimer(1000, 100) {
            override fun onFinish() {
                refresh(counterValue, "increment")
            }

            override fun onTick(p0: Long) {
                Log.d(TAG, "onTick: $p0")
            }
        }

        inc.setOnClickListener {
            countdownTimer.cancel()
            counterValue++
            lastAction = "increment"
            countdownTimer.start()
        }

        dec.setOnClickListener {
            countdownTimer.cancel()
            counterValue--
            lastAction = "decrement"
            countdownTimer.start()
        }
        refresh(action = lastAction)
    }

    private fun refresh(delta: Int = 0, action: String) {
        inc.isEnabled = false
        dec.isEnabled = false
        (application as App).api.counter(delta, action)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.isSuccessful) {
                        val valueResult = it.body().value
                        value.text = valueResult.toString()
                        inc.isEnabled = true
                        dec.isEnabled = true
                    } else {
                        Toast.makeText(this, getString(R.string.loading_error), Toast.LENGTH_SHORT).show()
                    }
                }, {
                    Log.e(TAG, "get data error: ", it)
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }, {
                    counterValue = 0
                })

    }


    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy: ")
        countdownTimer.cancel()
    }
}