package net.ivanov.tasks.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.Toast
import com.vicpin.krealmextensions.create
import com.vicpin.krealmextensions.deleteAll
import com.vicpin.krealmextensions.query
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_cities.*
import kotlinx.android.synthetic.main.city_view.view.*
import net.ivanov.tasks.App
import net.ivanov.tasks.Cities
import net.ivanov.tasks.R
import net.ivanov.tasks.db.CityModel


class CitiesActivity : AppCompatActivity() {
    val TAG = "CitiesActivity"
    private lateinit var loadCities: ICitiesLoad

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cities)
        title = "Cities"
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val linearLayoutManager = LinearLayoutManager(this)
        rv.layoutManager = linearLayoutManager
        val mDividerItemDecoration = DividerItemDecoration(
                rv.context,
                linearLayoutManager.orientation
        )
        rv.addItemDecoration(mDividerItemDecoration)

        repeat.setOnClickListener {
            load(true)
            error.visibility = View.GONE
            progress.visibility = View.VISIBLE
        }
        loadCities = object : ICitiesLoad {
            override fun onLoadMore() {
                load()
            }
        }
        load(true)

    }

    private var start = 0
    private fun load(firstLoad: Boolean = false) {
        val list = CityModel().query { it.equalTo("page", start) }
        if (list.isNotEmpty()) {
            updateList(firstLoad, list)
        } else {
            (application as App).api.getCities(start)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it.isSuccessful) {
                            val body = it.body()
                            if (body.pages < start)
                                return@subscribe
                            Log.d(TAG, " start $start body: $body")
                            saveCities(body)
                            updateList(firstLoad, body.cities.map { CityModel(0, city = it, page = 0, saved = false) })
                        } else {
                            showError(firstLoad)
                        }
                    }, {
                        Log.e(TAG, "onLoadMore: error", it)
                        showError(firstLoad)
                    })
        }
    }

    private fun saveCities(body: Cities) {
        Observable.fromCallable {
            val time = System.nanoTime()
            body.cities.forEachIndexed { index, city ->
                CityModel(time + index, city = city, page = body.page, saved = true).create()
            }
        }.subscribeOn(Schedulers.io())
                .subscribe({ Log.d(TAG, "saveCities: ") }, { Log.e(TAG, "saveCities: error", it) })
    }

    private fun updateList(firstLoad: Boolean, list: List<CityModel>) {
        if (firstLoad) {
            Log.d(TAG, "updateList: firstLoad")
            progress.visibility = View.GONE
            rv.visibility = View.VISIBLE
            rv.adapter = CitiesAdapter(loadCities, list.toMutableList(), rv)
        } else {
            loadDataInAdapter(list)
        }
        start++
    }

    private fun loadDataInAdapter(list: List<CityModel>) {
        Log.d(TAG, "loadDataInAdapter: list size=${list.size}")
        val adapter = rv.adapter as? CitiesAdapter?
        if (adapter != null) {
            if (list.isNotEmpty()) {
                list.forEach { adapter.addItem(it) }
                adapter.loading = false
            }
        }
    }

    private fun showError(firstLoad: Boolean) {
        if (firstLoad) {
            progress.visibility = View.GONE
            error.visibility = View.VISIBLE
        } else {
            Toast.makeText(this, getString(R.string.loading_error), Toast.LENGTH_SHORT).show()
        }
        (rv.adapter as? CitiesAdapter)?.loading = false
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.delete_db) {
            CityModel().deleteAll()
            start = 0
            rv.visibility = View.GONE
            progress.visibility = View.VISIBLE
            load(true)
        }
        return super.onOptionsItemSelected(item)
    }


}

class CitiesAdapter(val load: ICitiesLoad, private val list: MutableList<CityModel>, recyclerView: RecyclerView) : RecyclerView.Adapter<CitiesAdapter.ViewHolder>() {
    private var totalItemCount: Int = 0
    private var lastVisibleItem: Int = 0
    private val visibleThreshold = 3
    var loading: Boolean = false

    val TAG = "CitiesAdapter"

    init {
        if (recyclerView.layoutManager is LinearLayoutManager) {

            val linearLayoutManager = recyclerView
                    .layoutManager as LinearLayoutManager

            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView?,
                                        dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    totalItemCount = linearLayoutManager.itemCount
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                    if (!loading && totalItemCount <= lastVisibleItem + visibleThreshold) {
                        if (dy != 0) {
                            Log.d(TAG, "onScrolled: load More $loading ")
                            loading = true
                            this@CitiesAdapter.load.onLoadMore()
                        }
                    }
                }
            })
        }

    }


    fun addItem(city: CityModel) {
        list.add(city)
        notifyItemInserted(list.size)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.city_view, parent, false)
        return ViewHolder(v as LinearLayout)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cityModel = list[position]
        with(holder.ll) {
            city_name.text = cityModel.city
            img.visibility = if (cityModel.saved) View.VISIBLE else View.GONE
        }
    }

    override fun getItemCount(): Int = list.size

    open class ViewHolder(val ll: LinearLayout) : RecyclerView.ViewHolder(ll)


}

interface ICitiesLoad {
    fun onLoadMore()
}
