package net.ivanov.tasks.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import net.ivanov.tasks.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        task1.setOnClickListener {
            startActivity(Intent(this, UserActivity::class.java))
        }
        task2.setOnClickListener {
            startActivity(Intent(this, CitiesActivity::class.java))

        }
        task3.setOnClickListener {
            startActivity(Intent(this, DatesActivity::class.java))

        }
        task4.setOnClickListener {
            startActivity(Intent(this, CounterActivity::class.java))

        }
    }
}
