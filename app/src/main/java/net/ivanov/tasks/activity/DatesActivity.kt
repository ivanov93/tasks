package net.ivanov.tasks.activity

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_dates.*
import kotlinx.android.synthetic.main.dates_fragment.*
import kotlinx.android.synthetic.main.dates_view.view.*
import net.ivanov.tasks.*
import java.util.*
import kotlin.concurrent.timerTask


class DatesActivity : AppCompatActivity() {
    val TAG = "DatesActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dates)
        title = "Dates"
        supportActionBar?.setDisplayShowHomeEnabled(true)
        vp.offscreenPageLimit = 2
        tl.setupWithViewPager(vp)

        srl.setOnRefreshListener {
            loadDates()
        }

        repeat.setOnClickListener {
            loadDates(true)
        }

        vp.setOnTouchListener({ _, event ->
            srl.isEnabled = false
            when (event.action) {
                MotionEvent.ACTION_UP -> srl.isEnabled = true
            }
            false
        })
        loadDates(true)
    }

    private fun loadDates(firstLoaded: Boolean = false) {
        srl.isRefreshing = true
        error.visibility = View.GONE
        (application as App).api.getDates()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.isSuccessful) {
                        setupViewPager(vp, it.body())
                    } else {
                        if (firstLoaded) {
                            error.visibility = View.VISIBLE
                        } else {
                            Toast.makeText(this, getString(R.string.loading_error), Toast.LENGTH_SHORT).show()
                        }
                    }
                    srl.isRefreshing = false
                }, {
                    Log.e(TAG, "loadDates: error", it)
                    srl.isRefreshing = false
                })
    }


    private fun setupViewPager(viewPager: ViewPager, body: Dates) {
        val adapter = ViewPagerAdapter(supportFragmentManager).apply {
            addFragment(PastDatesFragment().withArray(body.past), "Past")
            addFragment(PastDatesFragment().withArray(body.future), "Future")
        }
        viewPager.adapter = adapter
    }


}

fun Fragment.withArray(list: List<DateEvent>): Fragment {
    return this.apply {
        arguments = android.os.Bundle().apply {
            putParcelable("list", net.ivanov.tasks.ParcDateEventList().apply { this.list = list })
        }
    }
}


class ViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {


    private val fragmentList = mutableListOf<Fragment>()
    private val fragmentTitleList = mutableListOf<String>()

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return fragmentTitleList[position]
    }

    fun addFragment(fragment: Fragment, title: String) {
        fragmentList.add(fragment)
        fragmentTitleList.add(title)
    }

}

class PastDatesFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.dates_fragment, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val linearLayoutManager = LinearLayoutManager(context)
        rv.layoutManager = linearLayoutManager
        val mDividerItemDecoration = DividerItemDecoration(
                rv.context,
                linearLayoutManager.orientation
        )
        rv.addItemDecoration(mDividerItemDecoration)
        rv.adapter = DatesAdapter(arguments.getParcelable<ParcDateEventList>("list").list)

    }

}

class DatesAdapter(private val list: List<DateEvent>) : RecyclerView.Adapter<DatesAdapter.ViewHolder>() {

    private val handler = Handler()

    private val holderList = mutableListOf<ViewHolder>()
    private val updateTime = Runnable {
        synchronized(holderList) {
            val currentTime = System.currentTimeMillis()
            holderList.forEach {
                it.updateTimeRemaining(currentTime)
            }
        }
    }

    init {
        startUpdateTimer()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.dates_view, parent, false)
        return ViewHolder(v as LinearLayout)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dateModel = list[position]
        synchronized(holderList) {
            holder.setData(dateModel)
            if (holderList.size < (list.size - 2)) holderList.add(holder)
        }
    }


    private fun startUpdateTimer() {
        val tmr = Timer()
        tmr.schedule(timerTask {
            handler.post(updateTime)
        }, 1000L, 1000L)
    }

    override fun getItemCount(): Int = list.size

    open class ViewHolder(private val ll: LinearLayout) : RecyclerView.ViewHolder(ll) {
        private lateinit var dateEvent: DateEvent
        fun setData(dateModel: DateEvent) {
            this.dateEvent = dateModel
            ll.event.text = dateModel.description
        }

        fun updateTimeRemaining(currentTime: Long) {
            val timeDiff = dateEvent.time * 1000 - currentTime
            val diffAbs = Math.abs(dateEvent.time * 1000 - currentTime)
            val seconds = (diffAbs / 1000).toInt() % 60
            val minutes = (diffAbs / (1000 * 60) % 60).toInt()
            val hours = (diffAbs / (1000 * 60 * 60) % 24).toInt()
            val day = (diffAbs / 86400000.0).toInt()
            var dayStr = day.toString()
            dayStr = if (day >= 100) dayStr.substring(dayStr.length - 2, dayStr.length) else dayStr
            val timeText = if (timeDiff < 0) "Expired" else "Remaining"
            ll.time.text = "$timeText $dayStr:$hours:$minutes:$seconds"
        }
    }
}


